import json
import hashlib
def get_url_key(url, seasoning):
    hex_url_key = hashlib.sha256(
        bytes(json.dumps({'url': url, 'seasoning': seasoning}), encoding='utf-8')
        ).hexdigest()
    id_set = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    value = int('0x' + hex_url_key, 16)
    final_key = ''
    n = len(id_set)
    while value:
        remainder = int(value % n)
        value = ( value - remainder ) / n
        final_key = id_set[remainder] + final_key
    return final_key