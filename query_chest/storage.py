import redis
import hashlib
import gzip
class Storage(object):
    def set(self, key, value) -> bool:
        raise NotImplementedError
    def get(self, key) -> str:
        raise NotImplementedError

class RedisStorage(Storage):
    def __init__(self, config) -> None:
        self.redis = redis.Redis(host=config['host'], port=config['port'])
        self.secret = config['secret']
        self.seasoning = config['seasoning']
    def set(self, key: str, value: str) -> bool:
        compressed = gzip.compress(bytes(str(value), 'utf-8'))
        return self.redis.set(self._get_secret_key(key), compressed)
    def get(self, key: str) -> str:
        res = self.redis.get(self._get_secret_key(key))
        if not res:
            return res
        try:
            decompressed = gzip.decompress(res)
        except:
            decompressed = res
        return decompressed.decode('utf-8')
    
    def _get_secret_key(self, key):
        hashed_key = hashlib.sha256(bytes(key + self.seasoning, encoding='utf-8')).hexdigest()
        return self.secret + ':' + hashed_key


def get_storage(config):
    if config['storage'] == 'redis':
        return RedisStorage(config['storageConfig'])
    raise RuntimeError