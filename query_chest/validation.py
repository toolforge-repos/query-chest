from urllib.parse import urlparse
from .storage import Storage
import time
class Validator(object):
    def validate_url(self, url):
        allowed_domains = ['query.wikidata.org', 'query-main.wikidata.org', 'query-scholarly.wikidata.org', 'commons-query.wikimedia.org']
        if not url.strip():
            return {'error': 'NO URL has been provided'}
        if len(url) > 10000:
            return {'error': 'URL is too long, current limit is 10000 chars'}
        try:
            pased_url = urlparse(url)
        except:
            return {'error': 'Invalid URL'}
        if pased_url.hostname not in allowed_domains:
            return {'error': 'Domain is not among allowed ones: ' + ','.join(allowed_domains)}
        if not pased_url.fragment:
            return {'error': 'No query to save'}

        return {'success': 1} 

    def rate_limit(self, storage: Storage):
        last_set = storage.get('last set')
        if not last_set:
            last_set = 0
        else:
            last_set = int(last_set)
        diff_secs = int(time.time()) - last_set
        if diff_secs < 20:
            return {'error': 'A query has been added too recently, please wait for {} seconds'.format(diff_secs)}
        
        storage.set('last set', int(time.time()))
        return {'success': 1}