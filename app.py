from flask import Flask, render_template, redirect, request
import os
import json
from urllib.parse import quote
from query_chest.storage import get_storage
from query_chest.validation import Validator
from query_chest.marshalling import get_url_key
app = Flask(__name__)
dir = os.path.dirname(os.path.abspath(__file__))
with open(os.path.join(dir, 'config.json'), 'r') as f:
    config = json.loads(f.read())
storage = get_storage(config)
validator = Validator()

@app.route("/redirect/<uuid>")
def redirect_(uuid):
    value = storage.get('urls:' + uuid)
    if not value:
        return render_template('error.html', error='Short URI does not exist')
    # redirect(json.loads(value)['url']) doesn't work with toolforge ngnix proxy
    return render_template('redirect.html', target=json.loads(value)['url'].replace('"', "&quot;"))


@app.route("/hold")
@app.route("/")
def hold():
    return render_template(
        'hold_get.html')

@app.route("/hold", methods=['POST'])
def hold_post():
    url = request.form.get('url', '').strip()
    url_validation = validator.validate_url(url)
    if 'error' in url_validation:
        return render_template('error.html', error=url_validation['error'])

    url_key = get_url_key(url, config['seasoning'])

    already_stored = storage.get('urls:' + url_key)
    if already_stored:
        return render_template('hold_post.html', short_url=config['base_url'] + '/redirect/' + url_key)
    
    rate_limit = validator.rate_limit(storage)
    if 'error' in rate_limit:
        return render_template('error.html', error=rate_limit['error'])

    stored = storage.set('urls:' + url_key, json.dumps({'url': url}))
    if stored:
        return render_template('hold_post.html', short_url=config['base_url'] + '/redirect/' + url_key)
    else:
        return render_template('error.html', error='Storage error, talk to sysadmin')


if __name__ == "__main__":
    app.run(host='0.0.0.0')
